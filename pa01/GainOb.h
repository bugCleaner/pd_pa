#ifndef GAINOB_H
#define GAINOB_H

#include "Cell.h"
#include <cstdlib>
#include <map>
#include <vector>

class GainOb {
	public :
		GainOb() ;
		int32_t gain;
		std::vector<Cell*> cells;


} ;

#endif