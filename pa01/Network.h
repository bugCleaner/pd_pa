#ifndef NETWORK_H
#define NETWORK_H

#include <map>
#include <string>
#include <vector>
#include <cstdint>
#include "Cell.h"
#include "Wire.h"
// #include "GainOb.h"


class Network {
    public :
        Network();
        ~Network();
        int8_t parsingInput(std::string,uint64_t ,uint64_t &) ;
        int8_t writeOutput(std::string) ;
        int8_t initGainList() ;
        int8_t RunFM() ;
        int8_t RunOneIte() ;
        void debugGain () ;
    private :
        // before move or after move?
        int8_t checkBalance(Cell*) ;
        int8_t moveCells(Cell*) ;
        // before move or after move?
        int8_t updataGain(Cell*) ;
        int32_t calGain(Cell*) ;
        int8_t resetToBest() ;
        int8_t setBest() ;
        int8_t removeFromBucketList(Cell*) ;
        int8_t insertToBucketList(Cell*) ;
        int8_t isMoreBalance() ;
        void debugParser(std::string) ;

        // debug series
        int8_t debug_wire_sides(Wire*,uint64_t,uint64_t) ;
        int8_t debug_bucket_list_size() ;
        int8_t debug_related_gain_of_cell(Cell*);
        int8_t debug_cutNUM() ;

        std::map<std::string, Cell*> map_cells;
        std::vector<Wire*> vector_wires;
        std::vector<Cell*> gainList;

        double r ;                          // balance ratio
        int64_t max_cell_pin ;
        int64_t total_weight ;
        int64_t max_partial_sum ;
        int64_t partial_sum ;       
        int64_t numSide0 ;
        int64_t numSide1 ;
        int64_t max_numSide0 ;
        int64_t max_numSide1 ;
        int64_t numCut ;
        int64_t miniNumCut ;
        uint64_t cellNum ;
} ;

#endif