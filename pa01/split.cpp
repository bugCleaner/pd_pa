#include "split.h"

void SplitString(const std::string& s, std::vector<std::string>& v, const std::string& c)
{
    std::string::size_type position1, position2;
    position2 = s.find(c);
    position1 = 0;
    while(std::string::npos != position2)
    {
    if ( (position2-position1) > 0)
        v.push_back(s.substr(position1, position2-position1));
 
    position1 = position2 + c.size();
    position2 = s.find(c, position1);
    }
    if(position1 != s.length())
    v.push_back(s.substr(position1));
}