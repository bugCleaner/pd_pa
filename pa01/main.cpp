#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include "Network.h"
#include <time.h>
#include <iomanip>

using namespace std ;


int main(int argc, char const *argv[])
{

    clock_t t0,t1,t2,t3;
    t0 = clock() ;
    string name_f_in = string(argv[1]);
    string name_f_out = string(argv[2]);
    Network *N_ptr = new Network () ;
    uint64_t c_num = 0 ;
    uint64_t threshold = 0 ;
    N_ptr->parsingInput(name_f_in,threshold,c_num) ;
    delete N_ptr ;
    threshold = c_num / 2 ;
    c_num = 0 ;
    Network network ;
    network.parsingInput(name_f_in,threshold,c_num) ;
    t1 = clock() ;
    network.RunFM();
    t2 = clock() ;
    // network.debugGain();
    network.writeOutput(name_f_out);
    t3 = clock() ;
    cout << "\n\n" ;
    cout << setw(25) << left << setfill(' ') << "Runtime report:" << endl ;
    cout << setw(25) << left << setfill(' ') << "parsingInput: " << (t1-t0)/double(CLOCKS_PER_SEC) << " s"<< endl ;
    cout << setw(25) << left << setfill(' ') << "RunFM: " << (t2-t1)/double(CLOCKS_PER_SEC) << " s"<< endl ;
    cout << setw(25) << left << setfill(' ') << "writeOutput: " << (t3-t2)/double(CLOCKS_PER_SEC) << " s"<< endl ;
    cout << setw(25) << left << setfill(' ') << "Total:   " << (t3-t0)/double(CLOCKS_PER_SEC) << " s"<< endl ;
    cout << "\n\n" ; 
    
    return 0;
}