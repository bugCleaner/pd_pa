#ifndef CELL_H
#define CELL_H

#include <string>
#include <vector>
#include <cstdint>
#include "Wire.h"

class Wire ;

class Cell {
    public :
    Cell (std::string in_s , uint8_t in_side ) { 
    	this->name = in_s ; 
    	this->side = in_side ; 
    	this->bestSide = in_side ;
    	this->islock = 0 ;
        this->inBucket = 0 ;
    	this->bestGain = 0 ;
    	this->gain = 0 ;
    	this->front = nullptr ;
    	this->back = nullptr ;
    }
    std::vector<Wire*> wires ;
    std::string name ;
    uint8_t side : 1;     
    uint8_t bestSide : 1;
    uint8_t islock : 1;
    uint8_t inBucket : 1;
    int16_t bestGain;
    int16_t gain ;  // Maybe 8bit is enough
    Cell *front ;
    Cell *back ;
} ;

#endif