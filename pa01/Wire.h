#ifndef WIRE_H
#define WIRE_H

#include <string>
#include <vector>
#include <cstdint>
#include "Cell.h"

class Cell ;

class Wire {
    public :
    Wire(std::string s) { 
    	this->name = s ; 
    	this->netSide0 = 0 ;
    	this->netSide1 = 0 ;
    	this->netBestSide0 = 0 ;
    	this->netBestSide1 = 0 ;}
    std::string name ;
    std::vector<Cell*> cells;
    uint16_t netSide0 ;
    uint16_t netSide1 ;
    uint16_t netBestSide0 ;
    uint16_t netBestSide1 ;

} ;

#endif