#include "Network.h"
#include <fstream>
#include "split.h"
#include <iostream>
#include <iomanip>

using namespace std ;

Network::Network(){ 
    /*TO-DO*/
    this->r = 0.0 ;
    this->max_cell_pin = 0 ;
    this->total_weight = 0 ;
    this->max_partial_sum = 0 ;
    this->partial_sum = 0 ;
    this->numSide0 = 0 ;
    this->numSide1 = 0 ;
    this->max_numSide0 = 0 ;
    this->max_numSide1 = 0 ;
    this->cellNum = 0 ;
    this->numCut = 0 ;
    this->miniNumCut = 0 ;
}


Network::~Network() {
    for ( std::map<std::string,Cell*>::iterator it = map_cells.begin() ; it != map_cells.end() ; ++it ) {
        Cell *c_ptr = it->second ;
        delete c_ptr ;
    }

    for ( uint64_t i = 0 ; i < vector_wires.size() ; i++ ) {
        Wire *w_ptr = vector_wires[i] ;
        delete w_ptr ;
    }
    map_cells.clear() ;
    vector_wires.clear() ;
    gainList.clear() ;
    // cout << "del Network" << endl ;
}


int8_t Network::parsingInput(string inputFileName , uint64_t threshold, uint64_t &arg_cell_num) {
    std::map<std::string,Cell*>::iterator iter;
    std::map<std::string,Cell*>::reverse_iterator iter_r;
    int8_t isFail = 0 ;
    fstream f_in ;
    f_in.open(inputFileName,ios::in) ;
    string buffer ;
    uint64_t line_index = 0 ;
    // exits NET before meet ";"
    uint8_t isStart = 0 ;
    // after "NET"
    uint8_t isAfterNET = 0;
    Wire *w_ptr = nullptr ;
    Cell *c_ptr = nullptr ;
    string s_net = "NET" ;
    string s_end = ";" ;
    // record number of cell in both sides of a net
    uint64_t net_side0 = 0 ;
    uint64_t net_side1 = 0 ;

    /*
    *   This map is used to deal with the XXXX case 0
    */
    std::map<std::string,Cell*> mapV;

    while((getline(f_in,buffer) )&& (isFail == 0)) {
        // read balance ratio
        if (line_index == 0){
            this->r = stod(buffer);
            // cout << "ratio is :" << this->r << endl ;
            // break ;
        } else {
            vector<string> tokens ;
            string delimiter = " " ;
            SplitString(buffer,tokens,delimiter) ;
            for ( int i = 0 ; i < tokens.size() ; i++ ) {
                // error detection
                if ( ( w_ptr == nullptr ) && (s_net.compare(tokens[0]) != 0 ) ) {
                    cout  << "Error ! Does not read NET at first!" << endl ;
                    isFail = 1 ;
                    break ;
                }

                // error detection
                if ( (s_net.compare(tokens[i]) == 0 ) && (isStart != 0) ) {
                    cout << "Error ! read two Net simultaneously!" << endl ;
                    isFail = 1 ;
                    break ; 
                }

                // next tokens is name of Wire
                if (s_net.compare(tokens[i]) == 0 ) {
                    isAfterNET = 1 ;
                    isStart = 1 ;
                    continue ;
                }

                // create a new wire
                if ( isAfterNET == 1  ) {
                    w_ptr = new Wire(tokens[i]) ;
                    this->vector_wires.push_back(w_ptr) ;
                    isAfterNET = 0 ;
                    net_side0 = 0 ;
                    net_side1 = 0 ;
                    continue ;
                }

                // meet ";" or add Cells into Wires
                if ( isStart == 1 ) {
                    if (s_end.compare(tokens[i]) == 0) {
                        // End of a net, That is, meet ";"
                        isStart = 0 ;
                        mapV.clear();
                        /* 
                        *   calculate gain caused by last net
                        */

                        // For updateGain improvement
                        w_ptr->netSide0 = net_side0 ;
                        w_ptr->netSide1 = net_side1 ;
                        w_ptr->netBestSide0 = w_ptr->netSide0 ;
                        w_ptr->netBestSide1 = w_ptr->netSide1 ;
                        // error detection
                        debug_wire_sides(w_ptr,net_side0,net_side1) ;


                        // special case one wire with only one cell
                        if ( (net_side0+net_side1) == 1 ){
                            continue ;
                        }

                        // Calculate the numCut caused by last net
                        if ( (net_side0 != 0) && (net_side1 != 0) ){
                            this->numCut += 1 ;
                        }

                        // without cells in side 0
                        if (net_side0 == 0) {
                            for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ) {
                                // error detection
                                if (w_ptr->cells[j]->side == 0){
                                    cout << "Error: net_side0 == 0 " << endl ;
                                }
                                w_ptr->cells[j]->gain -= 1 ;
                            }
                            continue ;
                        }

                        // without cells in side 1
                        if(net_side1 == 0) {
                            for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ) {
                                // error detection
                                if (w_ptr->cells[j]->side == 1 ) {
                                    cout << "Error: net_side1 == 0" << endl ;
                                }
                                w_ptr->cells[j]->gain -= 1 ;
                            }
                            continue ;
                        }


                        // only one in side 0
                        if (net_side0 == 1) {
                            uint32_t assign_num = 0 ;
                            for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ) {
                                if (w_ptr->cells[j]->side == 0 ) {
                                    w_ptr->cells[j]->gain += 1 ;
                                    assign_num++ ;
                                }
                            }
                            // error detection
                            if (assign_num != 1) {
                                cout << "Error: net_side0 == 1" << endl ;
                            }
                        }

                        // only one in side 1
                        if (net_side1 == 1){
                            uint32_t assign_num = 0 ;
                            for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ) {
                                if (w_ptr->cells[j]->side == 1 ) {
                                    w_ptr->cells[j]->gain += 1 ;
                                    assign_num++ ;
                                }
                            }
                            // error detection
                            if (assign_num != 1) {
                                cout << "Error: net_side1 == 1" << endl ;
                            }
                        }

                        // move to next token (wait for "NET")
                        continue;

                    } else {
                        // Add cell into cells in wire
                        iter = map_cells.find(tokens[i]) ;
                        if (iter == map_cells.end() ) {
                            if ( threshold == 0 ) {
                                if ( ((vector_wires.size())%2) == 0 ) {
                                    c_ptr = new Cell (tokens[i],0) ;
                                    numSide0++ ;
                                    net_side0++ ;
                                } else {
                                    c_ptr = new Cell (tokens[i],1) ;
                                    numSide1++ ;
                                    net_side1++ ;
                                }
                            } else {
                                if ( cellNum < threshold ) {
                                    c_ptr = new Cell (tokens[i],0) ;
                                    numSide0++ ;
                                    net_side0++ ;
                                } else {
                                    c_ptr = new Cell (tokens[i],1) ;
                                    numSide1++ ;
                                    net_side1++ ;
                                }
                            }
                            
                            cellNum++ ;
                            map_cells.insert(std::pair<std::string,Cell*>(tokens[i],c_ptr));
                            mapV.insert(std::pair<std::string,Cell*>(tokens[i],c_ptr));
                            c_ptr->wires.push_back(w_ptr) ;
                            w_ptr->cells.push_back(c_ptr) ;
                        } else if (mapV.find(tokens[i]) == mapV.end()){
                            mapV.insert(std::pair<std::string,Cell*>(tokens[i],c_ptr));
                            c_ptr = iter->second ;
                            c_ptr->wires.push_back(w_ptr) ;
                            w_ptr->cells.push_back(c_ptr) ;
                            // record for gain update
                            if (c_ptr->side == 0){
                                net_side0++ ;
                            } else {
                                net_side1++ ;
                            }
                        }

                        // record for max pin size
                        if (max_cell_pin < c_ptr->wires.size()){
                            max_cell_pin = c_ptr->wires.size() ;
                        }
                    }
                }

            }

        }
        line_index++;
    }

    // update max_partial_sum partial_sum max_numSide0 max_numSide1 miniNumCut
    this->max_partial_sum =  0 ;
    this->partial_sum  = 0 ;
    this->max_numSide0 = this->numSide0 ;
    this->max_numSide1 = this->numSide1 ;
    this->miniNumCut = this->numCut ;
    // for second parsing
    arg_cell_num = cellNum ;
    f_in.close() ;
    // debugParser(inputFileName) ;
    if (threshold != 0) {
        cout << setw(25) << left << setfill(' ') << "partial_sum: " << partial_sum << endl ;
        cout << setw(25) << left << setfill(' ') << "max_cell_pin: " << max_cell_pin << endl ;
        cout << setw(25) << left << setfill(' ') << "cellNum: " << cellNum << endl ;
        cout << setw(25) << left << setfill(' ') << "map_cells.size(): " << map_cells.size() << endl ;
        cout << setw(25) << left << setfill(' ') << "vector_wires.size(): " << vector_wires.size() << endl ;
        cout << "after parsingInput: " << endl ;    
    }
    
    // debug_cutNUM() ;
    return isFail ;
}

void Network::debugParser(string f_name) {
    fstream f_debug ;
    f_debug.open("debug.dat",ios::out) ;
    f_debug << r << "\n" ;
    for ( int i = 0 ; i < vector_wires.size() ; i++ ) {
        f_debug << "NET "  ;
        if (f_name.compare("./input_pa1/input_0.dat") == 0)
            f_debug << setw(9) << setfill(' ') << left << vector_wires[i]->name ;
        else
            f_debug << vector_wires[i]->name << " " ;
        for ( int j = 0 ; j < vector_wires[i]->cells.size() ; j++) {
            f_debug << vector_wires[i]->cells[j]->name ;
            if ( ( j%10 == 9) && ( j != 0 ) ){
                f_debug << "\n" ;
                f_debug << "             " ;                
            }else{
                f_debug << ' ' ;
            }
        }
        f_debug << ";" << "\n" ;
    }
    f_debug.close() ;
}

int8_t Network::writeOutput(std::string outputFileName) { 
    int8_t isFail = 0 ;
    fstream f_out ;
    f_out.open(outputFileName,ios::out) ;

    /*
    *   workaround for bug in miniNumCut
    */
    uint64_t tmp_cut_size = 0 ;
    for ( int i = 0 ; i < vector_wires.size() ; i++ ) {
        uint32_t n0 = 0 ;
        uint32_t n1 = 0 ;
        for ( int j = 0 ; j < vector_wires[i]->cells.size() ; j++ ){
            if ( vector_wires[i]->cells[j]->side == 0 )
                n0++ ;
            else
                n1++ ;
        }

        if ( (n0 != 0) && (n1 != 0))
            tmp_cut_size++ ;
    }

    f_out << "Cutsize = " << tmp_cut_size << endl ;
    f_out << "G1 " << numSide0 << endl ;
    for ( std::map<std::string,Cell*>::iterator it = map_cells.begin() ; it != map_cells.end() ; ++it ) {
        if ( it->second->side == 0){
            f_out << it->second->name << ' ' ;
        }
    }
    f_out << ';' << endl ;
    f_out << "G2 " << numSide1 << endl ;
    for ( std::map<std::string,Cell*>::iterator it = map_cells.begin() ; it != map_cells.end() ; ++it ) {
        if ( it->second->side == 1){
            f_out << it->second->name << ' ' ;
        }
    }
    f_out << ';' << endl ;
    f_out.close() ;
    /*TO-DO*/
    return isFail ;
}

int8_t Network::initGainList() {
    this->max_partial_sum =  0 ;
    this->partial_sum  = 0 ;
    setBest() ;
    /*TO-DO*/
    int8_t isFail = 0 ;
    std::map<std::string,Cell*>::iterator iter;
    std::map<std::string,Cell*>::reverse_iterator iter_r;
    int size_of_bucketlist = max_cell_pin*2 +1 ;
    /*
    *   initialize the bucketlist
    *   -N -(N-1) ... -2 -1 0 1 2 ... N-1  N
    */ 
    gainList.clear() ;
    for ( int i = 0 ; i < size_of_bucketlist ; i++ ) {
        gainList.push_back(nullptr) ;
    }

    // Traverse all elements int map of cells to establish double linklist
    for ( std::map<std::string,Cell*>::iterator it = map_cells.begin() ; it != map_cells.end() ; ++it ) {
        // error detection
        if (  ( (it->second->gain + max_cell_pin) >= size_of_bucketlist ) || ( (it->second->gain + max_cell_pin) < 0 )  ) {
            cout << "Error: ( (it->second->gain + max_cell_pin) >= size_of_bucketlist ) || ( (it->second->gain + max_cell_pin) < 0 )" << endl ;
        }

        if ( gainList[it->second->gain + max_cell_pin] == nullptr ) {
            gainList[it->second->gain + max_cell_pin] = it->second ;
            it->second->front = nullptr ;
            it->second->back = nullptr ;
        } else {
            // push front
            (gainList[it->second->gain + max_cell_pin])->front = it->second ;
            it->second->back = gainList[it->second->gain + max_cell_pin] ;
            gainList[it->second->gain + max_cell_pin] = it->second ;
            it->second->front = nullptr ;
        }
        // reset islock
        it->second->islock = 0 ;
        it->second->inBucket = 1 ;
    }

    // error detection
    debug_bucket_list_size() ;    

    return isFail ;
}
int8_t Network::checkBalance(Cell *c_ptr) {
    /*TO-DO*/
    /*
    *   This function will check whether or not the partition will stay balance after moving this cell
    */
    int8_t isFail = 0 ;
    double upperBound = (1.0+r)/2.0 ;
    double lowerBound = (1.0-r)/2.0 ;
    double A,B ;
    if (c_ptr->side == 0) {
        A = double(numSide0-1)/double(cellNum) ;
        B = double(numSide1+1)/double(cellNum) ;
    } else {
        A = double(numSide0+1)/double(cellNum) ;
        B = double(numSide1-1)/double(cellNum) ;
    }

    if ( (A > upperBound) || (A < lowerBound) ) {
        isFail = 1 ;
    }

    if ( (B > upperBound) || (B < lowerBound) ) {
        isFail = 1 ;
    }

    // cout << setw(20) << left << "numSide0:"  << numSide0 << endl ;
    // cout << setw(20) << left << "numSide1:"  << numSide1 << endl ;
    // cout << setw(20) << left << "cellNum:"  << cellNum << endl ;
    // cout << setw(20) << left << "upperBound:" << upperBound << endl ;
    // cout << setw(20) << left << "lowerBound:" << lowerBound << endl ;
    // cout << setw(20) << left << "A:" << A << endl ;
    // cout << setw(20) << left << "B:" << B << endl ;

    return isFail ;
}
int8_t Network::moveCells(Cell *c_ptr) {
    // error detection
    // static uint64_t isFirst = 0 ;
    /*TO-DO*/
    int8_t isFail = 0 ;
    std::map<std::string,Cell*> mapV;

    // collect cells need to update their gain
    for ( int i = 0 ; i < c_ptr->wires.size() ; i++ ) {
        Wire *w_ptr = c_ptr->wires[i] ;
        uint64_t net_side0 = 0 ;
        uint64_t net_side1 = 0 ;
        for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ) {
            if ( (w_ptr->cells)[j]->side == 0 ){
                net_side0++ ;
            } else {
                net_side1++ ;
            }
        }

        if ( (net_side1 <= 1)||(net_side0 <= 1)||((c_ptr->side == 1)&&(net_side1 <= 2))||((c_ptr->side == 0)&&(net_side0 <= 2))  ) {
            for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ) {
                mapV.insert(std::pair<std::string,Cell*>((w_ptr->cells)[j]->name,(w_ptr->cells)[j])) ;
            }
        }
    }

    // updating gain
    removeFromBucketList(c_ptr) ;
    this->partial_sum += c_ptr->gain ;
    this->numCut -= c_ptr->gain ;
    c_ptr->islock = 1;
    if (c_ptr->side == 0){
        c_ptr->side = 1 ;
        this->numSide0 -= 1 ;
        this->numSide1 += 1 ;
    } else {
        c_ptr->side = 0 ;
        this->numSide0 += 1 ;
        this->numSide1 -= 1 ;
    }
    c_ptr->gain = calGain(c_ptr) ;

    for ( std::map<std::string,Cell*>::iterator it = mapV.begin() ; it != mapV.end() ; ++it ){
        if (it->second->inBucket == 1){
            removeFromBucketList(it->second);
        }
        it->second->gain = calGain(it->second) ;
        if ( it->second->islock == 0){
            insertToBucketList(it->second) ;
        }
    }

    mapV.clear() ;

    // error detection
    // isFirst++ ;
    // if ( isFirst > 20840) {
    //     cout << isFirst << "-th " << endl ;
    //     if (debug_cutNUM()){
    //         cout << "diagnosis report: " << c_ptr->name << endl ;
    //         cin.get() ;
    //     }    
    // }
    // cout << c_ptr->name << endl ;
    
    return isFail ;
}

int8_t Network::updataGain(Cell *c_ptr) {
    /*TO-DO*/
    int isFail = 0 ;
    std::map<std::string,Cell*> mapV;
    // for c_ptr
    removeFromBucketList(c_ptr) ;
    this->partial_sum += c_ptr->gain ;
    this->numCut -= c_ptr->gain ;
    c_ptr->islock = 1;


    for ( int i = 0 ; i < (c_ptr->wires).size() ; i++  ) {
        Wire *w_ptr = c_ptr->wires[i] ;

        // For special case
        if ((w_ptr->netSide0 + w_ptr->netSide1 ) <= 1 ) {
            uint16_t tmp = w_ptr->netSide0 ;
            w_ptr->netSide0 = w_ptr->netSide1 ;
            w_ptr->netSide1 = tmp ;
            continue ;
        }

        if ( c_ptr->side == 0) {
            if ((w_ptr->netSide0 == 1) && (w_ptr->netSide1 == 1)) {
                for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ) {
                    if (w_ptr->cells[j]->inBucket == 1 ) {
                        removeFromBucketList(w_ptr->cells[j]);
                    }
                    if (w_ptr->cells[j] != c_ptr ) {
                        mapV.insert(std::pair<std::string,Cell*>((w_ptr->cells)[j]->name,(w_ptr->cells)[j]));
                    }
                    w_ptr->cells[j]->gain -= 2 ;
                }
            } else if ((w_ptr->netSide0 == 1) && (w_ptr->netSide1 > 1)) {
                for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ) {
                    if (w_ptr->cells[j]->inBucket == 1 ) {
                        removeFromBucketList(w_ptr->cells[j]);
                    }
                    if (w_ptr->cells[j] != c_ptr ) {
                        mapV.insert(std::pair<std::string,Cell*>((w_ptr->cells)[j]->name,(w_ptr->cells)[j]));
                        w_ptr->cells[j]->gain -= 1 ;
                    } else {
                        w_ptr->cells[j]->gain -= 2 ;
                    }
                }
            } else if ((w_ptr->netSide0 == 2) && (w_ptr->netSide1 == 0)) {
                for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ) {
                    if (w_ptr->cells[j]->inBucket == 1 ) {
                        removeFromBucketList(w_ptr->cells[j]);
                    }
                    if (w_ptr->cells[j] != c_ptr ) {
                        mapV.insert(std::pair<std::string,Cell*>((w_ptr->cells)[j]->name,(w_ptr->cells)[j]));
                    }
                    w_ptr->cells[j]->gain += 2 ;
                }
            } else if ((w_ptr->netSide0 == 2) && (w_ptr->netSide1 == 1)) {
                for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ) {
                    if (w_ptr->cells[j]->inBucket == 1 ) {
                        removeFromBucketList(w_ptr->cells[j]);
                    }
                    if (w_ptr->cells[j] != c_ptr) {
                        mapV.insert(std::pair<std::string,Cell*>((w_ptr->cells)[j]->name,(w_ptr->cells)[j]));
                        if (w_ptr->cells[j]->side == 0) {
                            w_ptr->cells[j]->gain += 1;
                        } else {
                            w_ptr->cells[j]->gain -= 1;
                        }
                    } else {
                        // c_ptr gain doesn't change
                    }

                }
            } else if ((w_ptr->netSide0 == 2) && (w_ptr->netSide1 > 1)) {
                for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ) {
                    if ((w_ptr->cells[j]->inBucket == 1) && (w_ptr->cells[j]->side == 0)) {
                        removeFromBucketList(w_ptr->cells[j]);
                    }
                    if (w_ptr->cells[j] != c_ptr) {
                        if (w_ptr->cells[j]->side == 0) {
                            mapV.insert(std::pair<std::string,Cell*>((w_ptr->cells)[j]->name,(w_ptr->cells)[j]));
                            w_ptr->cells[j]->gain += 1 ;
                        }
                    } else {
                        // c_ptr gain doesn't change
                    }
                }
            } else if ((w_ptr->netSide0 > 2) && (w_ptr->netSide1 == 0)) {
                for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ) {
                    if (w_ptr->cells[j]->inBucket == 1) {
                        removeFromBucketList(w_ptr->cells[j]);
                    }
                    if (w_ptr->cells[j] != c_ptr) {
                        w_ptr->cells[j]->gain += 1 ;
                        mapV.insert(std::pair<std::string,Cell*>((w_ptr->cells)[j]->name,(w_ptr->cells)[j]));
                    } else {
                        w_ptr->cells[j]->gain += 2 ;
                    }
                }
            } else if ((w_ptr->netSide0 > 2) && (w_ptr->netSide1 == 1)) {
                for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ) {
                    if (w_ptr->cells[j]->inBucket == 1) {
                        removeFromBucketList(w_ptr->cells[j]);
                    }
                    if (w_ptr->cells[j] != c_ptr) {
                        if (w_ptr->cells[j]->side == 1) {
                            w_ptr->cells[j]->gain -= 1 ;
                            mapV.insert(std::pair<std::string,Cell*>((w_ptr->cells)[j]->name,(w_ptr->cells)[j]));
                        }
                    } else {
                        // c_ptr gain doesn't change
                    }
                }
            }
            w_ptr->netSide0 -= 1 ;
            w_ptr->netSide1 += 1 ;
        // This else will start fot c_ptr in side 1
        } else {
            if ((w_ptr->netSide1 == 1) && (w_ptr->netSide0 == 1)) {
                for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ) {
                    if (w_ptr->cells[j]->inBucket == 1 ) {
                        removeFromBucketList(w_ptr->cells[j]);
                    }
                    if (w_ptr->cells[j] != c_ptr ) {
                        mapV.insert(std::pair<std::string,Cell*>((w_ptr->cells)[j]->name,(w_ptr->cells)[j]));
                    }
                    w_ptr->cells[j]->gain -= 2 ;
                }
            } else if ((w_ptr->netSide1 == 1) && (w_ptr->netSide0 > 1)) {
                for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ) {
                    if (w_ptr->cells[j]->inBucket == 1 ) {
                        removeFromBucketList(w_ptr->cells[j]);
                    }
                    if (w_ptr->cells[j] != c_ptr ) {
                        mapV.insert(std::pair<std::string,Cell*>((w_ptr->cells)[j]->name,(w_ptr->cells)[j]));
                        w_ptr->cells[j]->gain -= 1 ;
                    } else {
                        w_ptr->cells[j]->gain -= 2 ;
                    }
                }
            } else if ((w_ptr->netSide1 == 2) && (w_ptr->netSide0 == 0)) {
                for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ) {
                    if (w_ptr->cells[j]->inBucket == 1 ) {
                        removeFromBucketList(w_ptr->cells[j]);
                    }
                    if (w_ptr->cells[j] != c_ptr ) {
                        mapV.insert(std::pair<std::string,Cell*>((w_ptr->cells)[j]->name,(w_ptr->cells)[j]));
                    }
                    w_ptr->cells[j]->gain += 2 ;
                }
            } else if ((w_ptr->netSide1 == 2) && (w_ptr->netSide0 == 1)) {
                for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ) {
                    if (w_ptr->cells[j]->inBucket == 1 ) {
                        removeFromBucketList(w_ptr->cells[j]);
                    }
                    if (w_ptr->cells[j] != c_ptr) {
                        mapV.insert(std::pair<std::string,Cell*>((w_ptr->cells)[j]->name,(w_ptr->cells)[j]));
                        if (w_ptr->cells[j]->side == 1) {
                            w_ptr->cells[j]->gain += 1;
                        } else {
                            w_ptr->cells[j]->gain -= 1;
                        }
                    } else {
                        // c_ptr gain doesn't change
                    }

                }
            } else if ((w_ptr->netSide1 == 2) && (w_ptr->netSide0 > 1)) {
                for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ) {
                    if ((w_ptr->cells[j]->inBucket == 1) && (w_ptr->cells[j]->side == 1)) {
                        removeFromBucketList(w_ptr->cells[j]);
                    }
                    if (w_ptr->cells[j] != c_ptr) {
                        if (w_ptr->cells[j]->side == 1) {
                            mapV.insert(std::pair<std::string,Cell*>((w_ptr->cells)[j]->name,(w_ptr->cells)[j]));
                            w_ptr->cells[j]->gain += 1 ;
                        }
                    } else {
                        // c_ptr gain doesn't change
                    }
                }
            } else if ((w_ptr->netSide1 > 2) && (w_ptr->netSide0 == 0)) {
                for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ) {
                    if (w_ptr->cells[j]->inBucket == 1) {
                        removeFromBucketList(w_ptr->cells[j]);
                    }
                    if (w_ptr->cells[j] != c_ptr) {
                        mapV.insert(std::pair<std::string,Cell*>((w_ptr->cells)[j]->name,(w_ptr->cells)[j]));
                        w_ptr->cells[j]->gain += 1 ;
                    } else {
                        w_ptr->cells[j]->gain += 2 ;
                    }
                }
            } else if ((w_ptr->netSide1 > 2) && (w_ptr->netSide0 == 1)) {
                for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ) {
                    if (w_ptr->cells[j]->inBucket == 1) {
                        removeFromBucketList(w_ptr->cells[j]);
                    }
                    if (w_ptr->cells[j] != c_ptr) {
                        if (w_ptr->cells[j]->side == 0) {
                            w_ptr->cells[j]->gain -= 1 ;
                            mapV.insert(std::pair<std::string,Cell*>((w_ptr->cells)[j]->name,(w_ptr->cells)[j]));
                        }
                    } else {
                        // c_ptr gain doesn't change
                    }
                }
            }
            w_ptr->netSide0 += 1 ;
            w_ptr->netSide1 -= 1 ;
        }
    }

    for ( std::map<std::string,Cell*>::iterator it = mapV.begin() ; it != mapV.end() ; ++it ) {
        if ( it->second->islock == 0){
            insertToBucketList(it->second) ;
        }
    }

    if (c_ptr->side == 0){
        c_ptr->side = 1 ;
        this->numSide0 -= 1 ;
        this->numSide1 += 1 ;
    } else {
        c_ptr->side = 0 ;
        this->numSide0 += 1 ;
        this->numSide1 -= 1 ;
    }


    mapV.clear() ;

    return isFail ;
}

int8_t Network::resetToBest() {
    int8_t isFail = 0 ;
    for ( std::map<std::string,Cell*>::iterator it = map_cells.begin() ; it != map_cells.end() ; ++it ) {
        it->second->side = it->second->bestSide ;
        it->second->gain = it->second->bestGain ;
    }

    for ( uint64_t i = 0 ; i < vector_wires.size() ; i++ ) {
        Wire *w_ptr = vector_wires[i] ;
        w_ptr->netSide0 = w_ptr->netBestSide0 ;
        w_ptr->netSide1 = w_ptr->netBestSide1 ;
    }


    this->partial_sum = this->max_partial_sum ;
    this->numCut = this->miniNumCut ;
    this->numSide0 = this->max_numSide0 ;
    this->numSide1 = this->max_numSide1 ;
    return isFail ;
}
int8_t Network::setBest() {
    /*TO-DO*/
    int8_t isFail = 0 ;
    for ( std::map<std::string,Cell*>::iterator it = map_cells.begin() ; it != map_cells.end() ; ++it ) {
        it->second->bestSide = it->second->side ;
        it->second->bestGain = it->second->gain ;
    }

    for ( uint64_t i = 0 ; i < vector_wires.size() ; i++ ) {
        Wire *w_ptr = vector_wires[i] ;
        w_ptr->netBestSide0 = w_ptr->netSide0 ;
        w_ptr->netBestSide1 = w_ptr->netSide1 ;
    }

    this->max_partial_sum = this->partial_sum ;
    this->miniNumCut = this->numCut ;
    this->max_numSide0 = this->numSide0 ;
    this->max_numSide1 = this->numSide1 ;
    return 0 ; 
}
int8_t Network::RunOneIte() {
    int8_t isFail = 0 ;
    /*TO-DO*/
    int64_t moveNum = 0 ;
    while(1){
        uint8_t isDone = 0 ;
        for( int i = gainList.size() - 1 ; ( (i >= 0) && (isDone == 0) ) ; i--) {
            Cell *c_ptr = gainList[i] ;
            while(c_ptr != nullptr) {
                if ((checkBalance(c_ptr) == 0 ) && (c_ptr->islock == 0) ) {
                    // To record maximum partial sum
                    if (c_ptr->gain < 0 )  {
                        // cout << "---------setBest" << endl ;
                        // debugGain();
                        if (max_partial_sum < partial_sum) {
                            // cout << "*before setBest: " << endl ;
                            // debug_cutNUM() ;
                            setBest();
                            // cout << "*after setBest: " << endl ;
                            // debug_cutNUM() ;
                        } else if (max_partial_sum == partial_sum) {
                            if (isMoreBalance()){
                                // cout << "before setBest: " << endl ;
                                // debug_cutNUM() ;
                                setBest();
                                // cout << "after setBest: " << endl ;
                                // debug_cutNUM() ;
                            }
                        }
                        // cout << "---------after_setBest" << endl ;
                        // debugGain();
                        // cout << "---------endOfTest\n\n" << endl ;
                    }
                    // cout << "moveNum: " << moveNum << endl ;
                    // debug_related_gain_of_cell(c_ptr) ;
                    updataGain(c_ptr) ;
                    // debug_related_gain_of_cell(c_ptr);
                    // debug_related_gain_of_cell(c_ptr) ;
                    moveNum++ ;
                    isDone = 1 ;
                    break ;
                }
                c_ptr = c_ptr->back ;
            }
        }

        if (isDone == 1) {
            continue ;
        } else {
            break ;
        }
    }
    
    if (max_partial_sum > partial_sum)
        resetToBest() ;
    else
        setBest() ;

    cout << setw(20) << left << setfill(' ') << "   moveNum: " << moveNum << endl ;
    return isFail ;
}
int8_t Network::RunFM() {
    int8_t isFail = 0 ;
    /*TO-DO*/
    int32_t iteTime = 0 ;
    int64_t lastMiniCut = miniNumCut ;
    do {
        initGainList();
        // debugGain();
        lastMiniCut = miniNumCut ;
        iteTime++ ;
        cout << "\n\n" << endl ;
        cout << setw(20) << left << setfill(' ') << "   Start " << setw(5) << setfill(' ') << right << iteTime  ;
        cout << "-th iteration" << endl ;
        RunOneIte() ;
        cout << setw(20) << left << setfill(' ') << "   Finish " << setw(5) << setfill(' ') << right << iteTime  ;
        cout << "-th iteration" << endl ;
        cout << setw(20) << left << setfill(' ') << "   miniNumCut:" << miniNumCut << endl ;
        // debugGain();
    } while( lastMiniCut > miniNumCut ) ;
    // error detection
    debug_cutNUM() ;
    cout << "Finish All Processing" << endl ;
    return isFail ;
}
int32_t Network::calGain(Cell *c_ptr) {
    /*TO-DO*/
    /*For debugging and alpha test*/
    int32_t new_gain = 0 ;
    uint64_t w_side_0 = 0 ;
    uint64_t w_side_1 = 0 ;
    for ( int i = 0 ; i < c_ptr->wires.size() ; i++ ) {
        w_side_0 = 0 ;
        w_side_1 = 0 ;
        Wire *w_ptr = c_ptr->wires[i] ;

        // for special case
        if ( w_ptr->cells.size() <= 1) {
            continue ;
        }

        for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ) {
            if ( (w_ptr->cells)[j]->side == 0 ) {
                w_side_0++ ;
            } else {
                w_side_1++ ;
            }
        }

        // calculate gain for this net
        if ( (w_side_0 == 0) || (w_side_1 == 0) ) {
            new_gain-- ;
            continue ;
        }

        if ( (w_side_0 == 1) && (c_ptr->side == 0)  ) {
            new_gain++ ;
            continue ;
        }

        if ( (w_side_1 == 1) && (c_ptr->side == 1) ) {
            new_gain++ ;
            continue ;
        }

    }
        

    // return new gain without implicit assignment
    return new_gain ;
}


void Network::debugGain () {
    /*
    *   To check if gain is wrong or not
    */
    uint64_t equal_num = 0 ;
    for ( std::map<std::string,Cell*>::iterator it = map_cells.begin() ; it != map_cells.end() ; ++it ) {
        if (it->second->gain != calGain(it->second) ) {
            cout << "Error : it->second->gain != calGain(it->second) " << endl ;
            cout << "* cell " << it->second->name <<" right gain :" << calGain(it->second) << endl ;
            cout << "* cell " << it->second->name <<" wrong gain :" << it->second->gain << endl ;
        } else {
            equal_num++ ;
        }
    }
    cout << setw(25) << left << setfill(' ') << "equal_num:" << equal_num << endl ;
}

int8_t Network::removeFromBucketList(Cell* c_ptr) {
    int8_t isFail = 0 ;
    /*
    *   This function will remove cell from bucket list by its gain.
    *   Please note that the gain of this cell must be the old one for reason
    *   of complexity of implementation 
    */
    // To check which bucketlist contains c_ptr before
    int32_t index = max_cell_pin + c_ptr->gain ;

    if ( c_ptr->front == nullptr ) {
        if ( c_ptr->back == nullptr ) {
            gainList[index] = nullptr ;
        } else {
            c_ptr->back->front = nullptr ;
            gainList[index] = c_ptr->back ;
        }
    } else {
        if ( c_ptr->back == nullptr ) {
            c_ptr->front->back = nullptr ;
        } else {
            c_ptr->front->back = c_ptr->back ;
            c_ptr->back->front = c_ptr->front ;
        }
    }

    c_ptr->front = nullptr ;
    c_ptr->back = nullptr ;
    c_ptr->inBucket = 0 ;

    return isFail ;
}


int8_t Network::insertToBucketList(Cell* c_ptr) {
    int8_t isFail = 0 ;
    /*
    *   This function will insert cell into bucketlist by its gain
    */
    int32_t index = max_cell_pin + c_ptr->gain ;

    if ( gainList[index] == nullptr ) {
        gainList[index] = c_ptr ;
        c_ptr->front = nullptr ;
        c_ptr->back = nullptr ;
    } else {
        gainList[index]->front = c_ptr ;
        c_ptr->back = gainList[index] ;
        gainList[index] = c_ptr ;
        c_ptr->front = nullptr ;
    }

    c_ptr->inBucket = 1 ;

    return isFail ;
}


int8_t Network::isMoreBalance() {
    int8_t moreBalance = 0 ;

    int64_t dif = numSide1 - numSide0 ;
    int64_t difMax = max_numSide1 - max_numSide0 ;
    dif = (dif > 0) ? dif : ((-1)*dif) ;
    difMax = (difMax > 0) ? difMax : ((-1)*difMax) ;
    if (dif < difMax){
        moreBalance = 1 ;
    }

    return moreBalance ;
}



/*
*   Following functions are used to debug
*/


// This function is used to check whether or not the program calculates partition right.
int8_t Network::debug_wire_sides(Wire *w_ptr,uint64_t w_side_0,uint64_t w_side_1) {
    int8_t isFail = 0 ;

    if ( (uint16_t(w_side_0) != (w_ptr->netSide0)) || (uint16_t(w_side_1) != (w_ptr->netSide1)) ){
        isFail = 1 ;
        std::cout << setw(30) << setfill(' ') << left << "Assertion: " << "Network::debug_wire_sides (type2)" << std::endl ;
    }

    for ( int i = 0 ; i < w_ptr->cells.size() ; i++ ) {
        if ( w_ptr->cells[i]->side == 0 )
            w_side_0-- ;
        else
            w_side_1-- ;
    }

    if ( (w_side_1 != 0) || (w_side_0 != 0) ){
        isFail = 1 ;
        std::cout << setw(30) << setfill(' ') << left << "Assertion: " << "Network::debug_wire_sides" << std::endl ;
    }


    return isFail ;
}

// This function is used to check whether or not the bucket list initializes correctly
int8_t Network::debug_bucket_list_size() {
    int8_t isFail = 0 ;
    uint64_t size_of_bucketlist = uint64_t(max_cell_pin)*2 + 1 ; 
    uint64_t num_traverse = 0 ;
    for ( uint64_t i = 0 ; i < size_of_bucketlist ; i++ ) {
        Cell *c_ptr = gainList[i] ;
        while (c_ptr != nullptr) {
            num_traverse++ ;
            if ( (c_ptr->gain + max_cell_pin) != i ) {
                std::cout << setw(30) << setfill(' ') << left << "Assertion: " << "Network::debug_bucket_list_size detects wrong gain" << std::endl ;
                isFail = 1 ;
            }
            c_ptr = c_ptr->back ;
        }
    }

    if (num_traverse != map_cells.size() ) {
        isFail = 1 ;
        std::cout << setw(30) << setfill(' ') << left << "Assertion: " << "Network::debug_bucket_list_size detects wrong number" << std::endl ;
    }

    return isFail ;
}


// This function is used to check the gain the cells related to c_ptr
int8_t Network::debug_related_gain_of_cell(Cell* c_ptr) {
    int8_t isFail = 0 ;
    for ( uint64_t i = 0 ; i < c_ptr->wires.size() ; i++ ) {
        uint64_t w_side_0 = 0 ;
        uint64_t w_side_1 = 0 ;
        Wire *w_ptr = c_ptr->wires[i] ;
        for ( uint64_t j = 0 ; j < w_ptr->cells.size() ; j++ ) {
            if (w_ptr->cells[j]->side == 0)
                w_side_0++ ;
            else
                w_side_1++ ;
        }

        if ( (w_side_0 < 5) || (w_side_1 < 5) ) {
            for ( uint64_t j = 0 ; j < w_ptr->cells.size() ; j++ ){
                if ( w_ptr->cells[j]->gain != calGain(w_ptr->cells[j]) ){
                    isFail = 1 ;
                }
            }
        }
    }
    if (isFail == 1)
        std::cout << setw(30) << setfill(' ') << left << "Assertion: " << "Network::debug_related_gain_of_cell detect wrong gain" << std::endl ;

    return isFail ;
}


int8_t Network::debug_cutNUM() {
    int8_t isFail = 0 ;
    int64_t cut_tmp = 0 ;
    for ( int i = 0 ; i < vector_wires.size() ; i++ ) {
        uint8_t n0 = 0 ;
        uint8_t n1 = 0 ;
        Wire *w_ptr = vector_wires[i] ;
        for ( int j = 0 ; j < w_ptr->cells.size() ; j++ ){

            if ( w_ptr->cells[j]->side == 0){
                n0++ ;
            } else {
                n1++ ;
            }

            if ( (j%6 == 5) || (j == (w_ptr->cells.size()-1)) ) {
                if ( ( n0 > 0 ) && ( n1 > 0) ) {
                    cut_tmp++ ;
                    break ;
                }
            }
        }
    }

    if ( cut_tmp != this->numCut){
        std::cout << setw(30) << left << setfill(' ') << "Assertion: " << "Network::debug_cutNUM detects wrong cut size" << std::endl ;
        isFail = 1 ;
    }

    return isFail ;
}

