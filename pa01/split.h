#ifndef SPLIT_H
#define SPLIT_H
#include <string>
#include <vector>


// For splitting string into token

void SplitString(const std::string& , std::vector<std::string>& , const std::string& );

#endif