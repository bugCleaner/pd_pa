#ifndef __TILE_H__
#define __TILE_H__

#include "routingdb.h"
#include "parser.h"
#include "tree.h"
#include <string>
#include <cstdlib>
#include <fstream>


// FLAG for connection
#define CLEAR       0
#define UP_SIDE     1
#define DOWN_SIDE   2
#define LEFT_SIDE   4
#define RIGHT_SIDE  8
#define END_POINT   16
#define MASK16      65535
#define MASK5       15

class tile {
    public :
        tile(short in_Gx,short in_Gy,int in_cap_h , int in_cap_v ) { 
            this->Gx = in_Gx ; 
            this->Gy = in_Gy ;
            this->last_Gx = this->Gx ;
            this->last_Gy = this->Gy ;
            this->connect = CLEAR ;
            this->left_cap = in_cap_h ;
            this->down_cap = in_cap_v ;
            this->left_num = 0 ;
            this->down_num = 0 ;
            this->isPass_left = 0 ;
            this->isPass_Down = 0 ;
            this->step = -1 ;
            this->h = -1 ;
            this->num_bend = 0 ;
            this->cost = 0 ;
        }

        void resetCost() { 
            this->step = -1 ; 
            this->h = -1 ; 
            this->num_bend = 0 ;
            this->cost = 0 ;
            this->last_Gx = this->Gx ;
            this->last_Gy = this->Gy ;
        }

        void resetALLConnection() {
            this->connect = CLEAR ;
        }

        void resetEndPointConnection() {
            // Left END_POINT unlcear
            this->connect &= MASK5 ;
        }

        void reservedEndPointConnection() {
            this->connect &= END_POINT ;
        }

        void resetIsPass() {
            this->isPass_left = 0 ;
            this->isPass_Down = 0 ;
        }

        void updateBoundary() {
            if ( isPass_left != 0 ) {
                left_num += 1;
            }

            if (isPass_Down != 0 ) {
                down_num += 1 ;
            }
        }

        // If Via exists in this tile
        int withVia() {
            if ( (connect >= 4 ) && ( (connect&3) != 0 ) )
                return 1 ;
            else
                return 0 ;
        }
        // x(column) of grid ; y(row) of grid 
        short Gx ;
        short Gy ;
        short last_Gx ;
        short last_Gy ;

        // For calculate cost
        int step ;
        int h ;
        int num_bend ;
        int cost ;

        // Record capacity and pass num
        int left_cap ;
        int down_cap ;
        int left_num ;
        int down_num ;

        // record connection
        int connect ;

        

        // record the boundary should be modified
        char isPass_left ;
        char isPass_Down ;
        
} ;



#endif