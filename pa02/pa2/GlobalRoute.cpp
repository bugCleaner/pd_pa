#include "GlobalRoute.h"
#include "tile.h"
#include <set>
#include <map>
#include <cstdlib>
#include <climits>

class priQueue {
    public :
        priQueue() { count = 0 ;} 
        ~priQueue() {
            for (std::map<uint64_t, set<tile*> >::iterator it = priorityQueue.begin() ; it != priorityQueue.end() ; ++it ){
                set<tile*> s = it->second ;
                s.clear();
            }
            priorityQueue.clear() ;
        }

        void insert(int cost,tile* t) {
            std::map<uint64_t, set<tile*> >::iterator iter;
            iter = priorityQueue.find(cost) ;

            if (iter == priorityQueue.end()) {
                priorityQueue.insert( std::pair<int,set<tile*> >(cost,set<tile*>()) ) ;
            }
            priorityQueue[cost].insert(t) ;
            count++ ;
        }

        tile* getTopAndPop() {
            set<tile*> &s = priorityQueue.begin()->second ;
            tile* t = *(s.begin()) ;
            s.erase(s.begin()) ;
            if (s.size() <= 0 ) {
                priorityQueue.erase(priorityQueue.begin()) ;
            }
            count-- ;
            return t ;
        }

        uint8_t isEmpty() {
            return (count <= 0 ) ? 1 : 0 ;
        }

    private :
        std::map<uint64_t, set<tile*> > priorityQueue;
        uint64_t count ;
} ;

extern RoutingDB db;

using namespace std ;

GlobalRoute::GlobalRoute() {
    num_of_x_grid = db.GetHoriGlobalTileNo();
    num_of_y_grid = db.GetVertiGlobalTileNo();
    num_of_cap_adj = db.GetCapacityAdjustNo();
    num_of_Net = db.GetNetNo();

    // establish graph
    graph.resize(num_of_x_grid) ;
    for ( int i = 0 ; i < num_of_x_grid ; i++) {
        graph[i].resize(num_of_y_grid) ;
        for ( int j = 0 ; j < num_of_y_grid ; j++ ) {
            graph[i][j] = new tile(i,j,db.GetLayerHoriCapacity(0),db.GetLayerVertiCapacity(1)) ;
        }
    }

    // adjust capacity
    for (int i = 0; i < num_of_cap_adj ; i++) {
        CapacityAdjust& ca = db.GetCapacityAdjust(i);
        if (ca.GetGx1() != ca.GetGx2() ) {
            if ( ca.GetGx1() < ca.GetGx2() ) {
                graph[ca.GetGx2()][ca.GetGy2()]->left_cap = ca.GetReduceCapacity() ;
            } else {
                graph[ca.GetGx1()][ca.GetGy1()]->left_cap = ca.GetReduceCapacity() ;
            }
        } else {
            if ( ca.GetGy1() < ca.GetGy2() ) {
                graph[ca.GetGx2()][ca.GetGy2()]->down_cap = ca.GetReduceCapacity() ;
            } else {
                graph[ca.GetGx1()][ca.GetGy1()]->down_cap = ca.GetReduceCapacity() ;
            }
        }
    }
}

GlobalRoute::~GlobalRoute() {
    for ( int i = 0 ; i < num_of_x_grid ; i++) {
        for ( int j = 0 ; j < num_of_y_grid ; j++ ) {
            delete graph[i][j] ;
        }
    }
}

string GlobalRoute::getRoute(short Gx1_,short Gy1_,short layer1,short Gx2_,short Gy2_,short layer2) {
    int Gx1 = db.CalCenterX(Gx1_) ;
    int Gx2 = db.CalCenterX(Gx2_) ;
    int Gy1 = db.CalCenterY(Gy1_) ;
    int Gy2 = db.CalCenterY(Gy2_) ;
    string result ;

    // Fix order of layers
    char change = 0 ;
    if ( Gx1 != Gx2 ) {
        if ( Gx1 < Gx2 ) {
            change = 0 ;
        } else {
            change = 1 ;
        }
    } else {
        if ( Gy1 != Gy2 ) {
            if ( Gy1 < Gy2 ) {
                change = 0 ;
            } else {
                change = 1 ;
            }
        } else {
            if (layer1 < layer2) {
                change = 0 ;
            } else {
                change = 1 ;
            }
        }
    }

    if ( change == 1 ) {
        int tmp_int = 0 ;

        tmp_int = Gx1 ;
        Gx1 = Gx2 ;
        Gx2 = tmp_int ;

        tmp_int = Gy1 ;
        Gy1 = Gy2 ;
        Gy2 = tmp_int ;

        short tmp_short = 0 ;
        tmp_short = layer1 ;
        layer1 = layer2 ;
        layer2 = tmp_short ;
    }


    result += "(" ;
    result += to_string(Gx1) ;
    result += ",";
    result += to_string(Gy1) ;
    result += ",";
    result += to_string(layer1) ;
    result += ")-(" ;
    result += to_string(Gx2) ;
    result += ",";
    result += to_string(Gy2) ;
    result += ",";
    result += to_string(layer2) ;
    result += ")" ;

    
    return result ;
}

int GlobalRoute::getBoundaryCost(short gx1,short gy1,short gx2,short gy2){
    int result = 0 ;
    int shiftBits = 0 ;
    if (gx1 != gx2 ) {
        if (gx1 > gx2) {
            if (graph[gx1][gy1]->left_cap <= 0) {
                shiftBits = graph[gx1][gy1]->left_num + 1 ;
            } else {
                shiftBits = (graph[gx1][gy1]->left_num + 1)/(graph[gx1][gy1]->left_cap) ;
            }
        } else {
            if (graph[gx2][gy2]->left_cap <= 0) {
                shiftBits = graph[gx2][gy2]->left_num + 1;
            } else {
                shiftBits = (graph[gx2][gy2]->left_num + 1)/(graph[gx2][gy2]->left_cap) ;
            }
        }
    } else {
        if ( gy1 > gy2 ) {
            if (graph[gx1][gy1]->down_cap <= 0) {
                shiftBits = graph[gx1][gy1]->down_num + 1;
            } else {
                shiftBits = (graph[gx1][gy1]->down_num + 1)/(graph[gx1][gy1]->down_cap) ;
            }
        } else {
            if (graph[gx2][gy2]->down_cap <= 0) {
                shiftBits = graph[gx2][gy2]->down_num + 1;
            } else {
                shiftBits = (graph[gx2][gy2]->down_num + 1)/(graph[gx2][gy2]->down_cap) ;
            }
        }
    }

    // shiftBits *= 2 ;

    if (shiftBits >= 30) {
        result = (1<<30) ;
    } else {
        result = (1<<shiftBits) ;
    }
    return result ;
}

uint8_t GlobalRoute::isExist(short Gx, short Gy) {
    if ( (Gx < 0) || (Gx >= num_of_x_grid) || (Gy < 0) || (Gy >= num_of_y_grid) ) {
        return 0 ;
    } else {
        return 1;
    }
    
}

int GlobalRoute::calManhattanDistance(short Gx1, short Gy1, short Gx2 , short Gy2 ) {
    int distance = 0 ;
    if ( Gx1 > Gx2 ) {
        distance += (Gx1 - Gx2) ;
    } else {
        distance += (Gx2 - Gx1) ;
    }

    if ( Gy1 > Gy2 ) {
        distance += (Gy1 - Gy2) ;
    } else {
        distance += (Gy2 - Gy1) ;
    }

    return distance ;
}

uint64_t GlobalRoute::routeAll(uint8_t mode,ofstream & file) {
    uint64_t failCount = 0 ;

    if (mode != 0) {
        std::cout << "Start to Route:\n" << std::endl ;
    }

    int oneTenth = (num_of_Net/10) + 1  ;

    // Route each Net
    for ( int i = 0 ; i < num_of_Net ; i++ ) {
        Net& n = db.GetNetByPosition(i);
        uint64_t fail = routeOneNet(mode,n,file) ;
        failCount += fail ;

        if ( mode == 1 ) {
            if ( (i%oneTenth) == 0 ) {
                std::cout << "  complete:" << (i+1) << "/" << num_of_Net << std::endl ;
            }
        } else if (mode == 2) {
            std::cout << "  complete:" << (i+1) << "/" << num_of_Net << std::endl ;
        }
    }

    if (mode != 0) {
        std::cout << "\nEnd to Route" << std::endl ;
    }


    return failCount ;
}

uint64_t GlobalRoute::routeOneNet(uint8_t mode,Net& n,ofstream & file) {
    uint64_t failCount = 0 ;

    // For output
    std::set<string> routeNets;
    std::set<tile*> routedTiles;

    // route Each subNet
    for ( int i = 0 ; i < n.GetSubNetNo() ; i++ ) {
        // To key is cost
        priQueue candidates;
        set<tile*> exploredTiles ;

        // two pin infromation
        SubNet& e = n.GetSubNet(i) ;
        short sourceGx = e.GetSourcePinGx() ;
        short sourceGy = e.GetSourcePinGy() ;
        short targetGx = e.GetTargetPinGx() ;
        short targetGy = e.GetTargetPinGy() ;

        // same tiles
        if (graph[targetGx][targetGy] == graph[sourceGx][sourceGy]) {
            cout << "graph[targetGx][targetGy] == graph[sourceGx][sourceGy]" << endl ;
            continue ;
        }

        // To determine start point
        if ( (graph[sourceGx][sourceGy]->connect & END_POINT) != 0 ) {
            if ( (graph[targetGx][targetGy]->connect & END_POINT) != 0 ) {
                cout << "two point have already connected" << endl ;
                continue ;
            } else {
                // swap start point and end point
                short tmp = 0 ;
                tmp = sourceGx ;
                sourceGx = targetGx ;
                targetGx = tmp ;

                tmp = sourceGy ;
                sourceGy = targetGy ;
                targetGy = tmp ;
            }
        }

        // initialization
        graph[sourceGx][sourceGy]->h = calManhattanDistance(sourceGx,sourceGy,targetGx,targetGy) + 0 ;
        graph[sourceGx][sourceGy]->step = 0 ;
        graph[sourceGx][sourceGy]->num_bend = 0 ;
        graph[sourceGx][sourceGy]->connect |= END_POINT ;
        graph[targetGx][targetGy]->connect |= END_POINT ;
        exploredTiles.insert(graph[sourceGx][sourceGy]) ;

        // push start point to priorityQueue
        int cost = graph[sourceGx][sourceGy]->h + graph[sourceGx][sourceGy]->step + graph[sourceGx][sourceGy]->num_bend;
        candidates.insert(cost,graph[sourceGx][sourceGy]) ;

        // route
        tile *cur ;
        while(candidates.isEmpty() != 1 ) {
            cur = candidates.getTopAndPop() ;

            // if we connect to endPoint net
            if ( ((cur->Gx != sourceGx) || (cur->Gy != sourceGy) ) &&  ((cur->connect & END_POINT) != 0 ) ) {
                break ;
            }

            std::vector<short> gxlist ;
            std::vector<short> gylist ;
            gxlist.push_back((cur->Gx) + 1) ;
            gxlist.push_back((cur->Gx) - 1) ;
            gxlist.push_back(cur->Gx) ;
            gxlist.push_back(cur->Gx) ;
            gylist.push_back(cur->Gy) ;
            gylist.push_back(cur->Gy) ;
            gylist.push_back((cur->Gy) + 1) ;
            gylist.push_back((cur->Gy) - 1) ;

            for ( int j = 0 ; j < 4 ; j++ ) {
                short x = gxlist[j] ;
                short y = gylist[j] ;
                if (isExist(x,y) == 1 ) {
                    tile *tmp = graph[x][y] ;
                    int new_h = calManhattanDistance(tmp->Gx,tmp->Gy,targetGx,targetGy) ;
                    int new_bend = 0 ;
                    int new_step = cur->step + 1 ;
                    int new_cost = cur->cost ;
                    // bend or not
                    int difx = tmp->Gx - cur->last_Gx ;
                    int dify = tmp->Gy - cur->last_Gy ;
                    difx = (difx > 0) ? difx : (-1*difx) ;
                    dify = (dify > 0) ? dify : (-1*dify) ;
                    if ( (difx >= 2) || (dify >= 2) || ((difx == 1) && (dify == 0)) ) {
                        new_bend = cur->num_bend ;
                    } else {
                        new_bend = cur->num_bend + 1 ;
                    }

                    if ((INT_MAX - getBoundaryCost(tmp->Gx,tmp->Gy,cur->Gx,cur->Gy) ) < (new_cost+new_step+new_bend+new_h) ) {
                        new_cost = INT_MAX - (new_cost+new_step+new_bend+new_h) ;
                    } else {
                        new_cost += getBoundaryCost(tmp->Gx,tmp->Gy,cur->Gx,cur->Gy) ;
                    }

                    if (exploredTiles.find(tmp) != exploredTiles.end()){
                        if ((tmp->step + tmp->h + tmp->num_bend + tmp->cost ) <= (new_step + new_h + new_bend + new_cost)) {
                            continue ;
                        }
                    } else {
                        exploredTiles.insert(tmp) ;
                        tmp->step = new_step ;
                        tmp->h = new_h ;
                        tmp->num_bend = new_bend ;
                        tmp->cost = new_cost ;
                        tmp->last_Gx = cur->Gx ;
                        tmp->last_Gy = cur->Gy ;
                        int totalCost = new_step + new_h + new_bend + new_cost ;
                        candidates.insert(totalCost,tmp) ;
                    }

                }
            }

        }

        // error detection
        if ( ((cur->connect & END_POINT) == 0 ) || (cur == graph[sourceGx][sourceGy]) ) {
            cin.get() ;
            cout << "Failed" << endl ;
        }

        // find a path (tracing from end pin to start pin)
        while ( cur != graph[sourceGx][sourceGy] ) {
            routedTiles.insert(cur) ;
            tile *next = graph[cur->last_Gx][cur->last_Gy] ;

            if (cur->Gx != next->Gx) {
                if (cur->Gx > next->Gx) {
                    cur->connect |= LEFT_SIDE ;
                    next->connect |= RIGHT_SIDE ;
                    cur->isPass_left = 1 ;  
                } else {
                    cur->connect |= RIGHT_SIDE ;
                    next->connect |= LEFT_SIDE ;
                    next->isPass_left = 1 ;
                }
                
                string s_tmp ;
                if (cur->withVia() != 0) {
                    s_tmp = getRoute(cur->Gx,cur->Gy,1,cur->Gx,cur->Gy,2) ;
                    routeNets.insert(s_tmp) ;
                }
                s_tmp = getRoute(cur->Gx,cur->Gy,1,next->Gx,next->Gy,1) ;
                routeNets.insert(s_tmp) ;
            } else {
                if (cur->Gy != next->Gy) {
                    if (cur->Gy > next->Gy) {
                        cur->connect |= DOWN_SIDE ;
                        next->connect |= UP_SIDE ;
                        cur->isPass_Down = 1 ;
                    } else {
                        cur->connect |= UP_SIDE ;
                        next->connect |= DOWN_SIDE ;
                        next->isPass_Down = 1 ;
                    }
                    string s_tmp ;
                    if (cur->withVia() != 0) {
                        s_tmp = getRoute(cur->Gx,cur->Gy,1,cur->Gx,cur->Gy,2) ;
                        routeNets.insert(s_tmp) ;
                    }
                    s_tmp = getRoute(cur->Gx,cur->Gy,2,next->Gx,next->Gy,2) ;
                    routeNets.insert(s_tmp) ;
                } else {
                    cout << "Find Path Error!" << endl ;
                }
            }

            cur = next ;
        }

        // deal with start point
        string s_tmp ;
        routedTiles.insert(cur) ;
        if (cur->withVia() != 0) {
            s_tmp = getRoute(cur->Gx,cur->Gy,1,cur->Gx,cur->Gy,2) ;
            routeNets.insert(s_tmp) ;
        }

        // clear explored tiles
        for (std::set<tile*>::iterator it = exploredTiles.begin(); it != exploredTiles.end() ; it++ ) {
            tile *p = *it ;
            p->resetALLConnection() ;
            p->resetCost() ;
        }

        // Mark routed Tiles as END_POINT
        for (std::set<tile*>::iterator it = routedTiles.begin(); it != routedTiles.end() ; it++ ){
            tile *p = *it ;
            p->connect |= END_POINT ;
            p->resetALLConnection() ;
        }


    }


    // update pass number of tiles
    for (std::set<tile*>::iterator it = routedTiles.begin(); it != routedTiles.end() ; it++ ) {
        tile *p = *it ;
        p->updateBoundary() ;
        p->resetALLConnection() ;
        p->resetCost() ;
        p->resetIsPass() ;
    }

    // Output routed result of this line
    file << n.GetName() << ' ' << n.GetUid() << " " << routeNets.size() << endl ;
    for (std::set<string>::iterator it = routeNets.begin(); it != routeNets.end() ; it++ ) {
        string s = *it ;
        file << s << endl ;
    }
    file << "!" << endl ;

    return failCount ;
}

