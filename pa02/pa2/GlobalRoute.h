#ifndef __GLOBALROUTE_H__
#define __GLOBALROUTE_H__

#include "routingdb.h"
#include "parser.h"
#include "tree.h"
#include <string>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <cstdint>

// predefine class type
class tile ;


class GlobalRoute {
    public :
        GlobalRoute(/*no argument because it will get information from db*/) ;
        /*
        mode 0  :   show no information
        mode 1  :   show rough information
        mode 0  :   show detail information
        */
        uint64_t routeAll(uint8_t,ofstream &) ;
        ~GlobalRoute() ;

    private:
        int calManhattanDistance(short , short , short , short ) ;
        uint8_t isExist(short, short ) ;
        uint64_t routeOneNet(uint8_t,Net&,ofstream &);
        string getRoute(short ,short ,short ,short ,short ,short ) ;
        int getBoundaryCost(short,short,short,short) ;
        short num_of_x_grid ;   // column number
        short num_of_y_grid ;   // row number
        int num_of_cap_adj ;  // adjustment count
        int num_of_Net ;
        vector< vector<tile*> > graph;



} ;


#endif