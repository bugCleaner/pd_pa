#ifndef __FLOORPLANNER_H__
#define __FLOORPLANNER_H__

#include <string>

#include "BStarTree.h"
#include <time.h>

class FloorPlanner {
    public :
        FloorPlanner() {t0 = clock() ;} 
        ~FloorPlanner() {/*Nothing TO-DO*/;} 
        void parsingBlockFile(std::string) ;
        void parsingNetFile(std::string) ; 
        void plotPicture(std::string pictureName) {
            bt.updatePosition() ;
            bt.plot(pictureName,outlineWidth,outlineHeight) ;
        } 
        void runSA(double t,double s) {
            // w:width  h:height  t:temperature  s:scale
            bt.SA(this->outlineWidth,this->outlineHeight,t,s) ;
        }
        void writeOutputFile(std::string) ;
        void setBlockFileName(std::string s) { this->blockFileName = s ;}
        void setNetFileName(std::string s) { this->NetFileName = s ;}
        void setalphaValue(double a) { this->alphaValue = a ;}
        double getLen() { return bt.getTotalWireLength() ;}
    private :
        BStarTree bt ;
        int blockNum ;
        int terminalNum ;
        int outlineWidth ;
        int outlineHeight ;
        double alphaValue ;
        int netNum ;
        std::string blockFileName ;
        std::string NetFileName ;
        clock_t t0,t1 ;
} ;


#endif