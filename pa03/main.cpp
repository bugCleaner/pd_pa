#include <iostream>
#include "FloorPlanner.h"
#include <string>

using namespace std ;

int main(int argc, char const *argv[])
{
    FloorPlanner fp ;
    /*
        argv[1] :   alpha value
        argv[2] :   input.block
        argv[3] :   input.nets
        argv[4] :   output file name
    */
    if (argc < 5 ) {
        std::cout << "Wrong number of argument" << std::endl ;
        return 0 ;
    }

    fp.setalphaValue(std::stod(argv[1])) ;
    fp.parsingBlockFile(argv[2]) ;
    fp.parsingNetFile(argv[3]);
    fp.runSA(1000,0.97) ;
    fp.writeOutputFile(argv[4]) ;
    std::string plotName ;
    if (argc == 6) {
        plotName = std::string(argv[5]) ;
    } else {
        plotName = "floorplan.png" ;
    }
    fp.plotPicture(plotName) ;

    std::cout << "END of Floorplanning" << std::endl ;

    return 0;
}