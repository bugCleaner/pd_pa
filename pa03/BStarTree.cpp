#include "BStarTree.h"
#include "Contour.h"
#include <cstdlib>
#include <fstream>
#include <cmath>
#include <limits>

BStarTree::BStarTree() {
    isEmpty = 1 ;
    blockCount = 0 ;
    root = nullptr ;
    bestRoot = nullptr ;
    outLineWidth = -1 ;
    outLineHeight = -1 ;
    maxX = -1 ;
    maxY = -1 ;
    bestCost = std::numeric_limits<double>::max() ;
    preCost = std::numeric_limits<double>::max() ;
    isInitBest = 0 ;
}

BStarTree::~BStarTree() {
    for ( int i = 0 ; i < blockList.size() ; i++ ) {
        bptr p = blockList[i] ;
        delete p ;
    }

    // release terminals memory
    for (std::map<std::string, bstpptr>::iterator it = TerminalMap.begin() ; it != TerminalMap.end() ; ++it ){
        bstpptr p = it->second ;
        delete p ;
    }
    // release net memory
    for ( int i = 0 ; i < netVector.size() ; i++ ) {
        nptr p = netVector[i] ;
        delete p ;
    }
}

void BStarTree::addTerminal(std::string s,int x_in,int y_in) {
    TerminalMap[s] = new BSTPoint(s,x_in,y_in) ;
}

double BStarTree::getTerminalX(std::string s) {
    std::map<std::string, bstpptr>::iterator it ;
    it = TerminalMap.find(s) ;
    if (it != TerminalMap.end()) {
        return static_cast<double>(it->second->getX()) ;
    } else {
        return -1.0 ;
    }
}

double BStarTree::getTerminalY(std::string s) {
    std::map<std::string, bstpptr>::iterator it ;
    it = TerminalMap.find(s) ;
    if (it != TerminalMap.end()) {
        return static_cast<double>(it->second->getY()) ;
    } else {
        return -1.0 ;
    }
}

int BStarTree::isTerminal(std::string s) {
    std::map<std::string, bstpptr>::iterator it ;
    it = TerminalMap.find(s) ;
    if (it != TerminalMap.end()) {
        return 1 ;
    } else {
        return 0 ;
    }
}

double BStarTree::getBlockX(std::string s) {
    std::map<std::string, bptr>::iterator it ;
    it = blockMap.find(s) ;
    if (it != blockMap.end()) {
        return static_cast<double>(it->second->getX()) + static_cast<double>(it->second->getWidth()) /2.0 ;
    } else {
        return -1.0 ;
    }
}

double BStarTree::getBlockY(std::string s) {
    std::map<std::string, bptr>::iterator it ;
    it = blockMap.find(s) ;
    if (it != blockMap.end()) {
        return static_cast<double>(it->second->getY()) + static_cast<double>(it->second->getHeight()) /2.0 ;
    } else {
        return -1.0 ;
    }
}

int BStarTree::isBlock(std::string s) {
    std::map<std::string, bptr>::iterator it ;
    it = blockMap.find(s) ;
    if (it != blockMap.end()) {
        return 1 ;
    } else {
        return 0 ;
    }
}

void BStarTree::addNet(std::vector<std::string> &names) {
    // This function can't not be called before reading *.block files
    nptr p = new NET() ;
    for ( int i = 0 ; i < names.size() ; i++ ) {
        if (isBlock(names[i]) == 1) {
            (p->blocks).push_back(names[i]) ;
        } else if (isTerminal(names[i]) == 1) {
            (p->terminals).push_back(names[i]) ;
        } else {
            std::cout << "Assertion: Bug in addNet" << std::endl ;
            std::cin.get() ;
        }
    }
    netVector.push_back(p) ;
}


double BStarTree::getTotalWireLength() {
    double totalLen = 0 ;
    for ( int i = 0 ; i < netVector.size() ; i++ ) {
        nptr p = netVector[i] ;
        p->reset() ;
        for ( int j = 0 ; j < p->blocks.size() ; j++) {
            double x,y ;
            x = getBlockX(p->blocks[j]) ;
            y = getBlockY(p->blocks[j]) ;

            if ((x== -1.0) || (y == -1.0)) {
                std::cout << "Assertion: bug in getTotalWireLength" << std::endl ;
                std::cin.get() ;
            }

            if (x > (p->maxX)) {
                p->maxX = x ;
            }

            if (x < (p->minX) ) {
                p->minX = x ;
            }

            if ( y > (p->maxY)) {
                p->maxY = y ;
            }

            if (y < (p->minY) ) {
                p->minY = y ;
            }

        }

        for ( int j = 0 ; j < p->terminals.size() ; j++) {
            double x,y ;
            x = getTerminalX(p->terminals[j]) ;
            y = getTerminalY(p->terminals[j]) ;

            if ((x== -1.0) || (y == -1.0)) {
                std::cout << "Assertion: bug in getTotalWireLength" << std::endl ;
                std::cin.get() ;
            }

            if (x > (p->maxX)) {
                p->maxX = x ;
            }

            if (x < (p->minX) ) {
                p->minX = x ;
            }

            if ( y > (p->maxY)) {
                p->maxY = y ;
            }

            if (y < (p->minY) ) {
                p->minY = y ;
            }

        }

        totalLen +=  (p->maxX - p->minX)  + (p->maxY - p->minY)  ;


    }
    return totalLen ;
}


void BStarTree::backtrackToBest() {

    // restore block information
    for ( int i = 0 ; i < blockList.size() ; i++ ) {
        bptr tmp = blockList[i] ;
        tmp->backToBest() ;
        // restore map
        blockMap[tmp->getName()] = tmp ;
    }

    root = bestRoot ;

}

void BStarTree::Block::setBest() {
    best_name  = name ;
    best_width = width ;
    best_height = height ;
    best_x = x ;
    best_y = y ;
    best_right = right ;
    best_up = up;
}

void BStarTree::Block::backToBest() {
    name  = best_name ;
    width = best_width ;
    height = best_height ;
    x = best_x ;
    y = best_y ;
    right = best_right ;
    up = best_up;
}


void BStarTree::setBSTreeBest() {
    isInitBest = 1 ;

    // restore block information
    for ( int i = 0 ; i < blockList.size() ; i++ ) {
        bptr tmp = blockList[i] ;
        tmp->setBest() ;
    }

    bestRoot = root ;
}

void BStarTree::addBlock(std::string n ,int w ,int h) {
    static int isFirst = 1 ;
    blockCount++ ;
    isEmpty = 0 ;
    if (isFirst == 1) {
        isFirst = 0 ;
        srand(43098) ;
    }
    bptr cur = root ;
    if (root == nullptr) {
        root = new Block(n,w,h) ;
        blockMap[n] = root ;
        blockList.push_back(root) ;
    } else {
        while(cur != nullptr) {
            int x = rand() ;
            if ((x%2) == 0) {
                if ((cur->getRight()) == nullptr) {
                    bptr p = new Block(n,w,h) ;
                    blockMap[n] = p ;
                    blockList.push_back(p) ;
                    cur->setRight(p) ;
                    break ;
                } else {
                    cur = cur->getRight() ;
                }
            } else {
                if ((cur->getUp()) == nullptr ) {
                    bptr p = new Block(n,w,h) ;
                    blockMap[n] = p ;
                    blockList.push_back(p) ;
                    cur->setUp(p) ;
                    break ;
                } else {
                    cur = cur->getUp() ;
                }

            }
        }
    }


}


void BStarTree::updatePosition() {
    maxX = -1 ;
    maxY = -1 ;
    Contour c ;
    bptr cur = root ;
    root->setX(0) ;
    int y = c.addHC(0,(0 + root->getWidth()),root->getHeight());
    root->setY(y) ;
    updateBlockRecur(0+(root->getWidth()),root->getRight(),c);
    updateBlockRecur(0,root->getUp(),c);

    if ((0 + root->getWidth()) > maxX) {
        maxX = 0 + root->getWidth() ;
    }

    if ((0 + root->getHeight()) > maxY) {
        maxY = 0 + root->getHeight() ;
    }


}

void BStarTree::updateBlockRecur(int x,bptr p,Contour& c) {
    if (p != nullptr) {
        p->setX(x) ;
        int y = c.addHC(x,(x + p->getWidth()),p->getHeight());
        p->setY(y) ;
        updateBlockRecur(x+(p->getWidth()),p->getRight(),c);
        updateBlockRecur(x,p->getUp(),c);   

        if ((x + p->getWidth()) > maxX) {
            maxX = x + p->getWidth() ;
        }

        if ((y + p->getHeight()) > maxY) {
            maxY = y + p->getHeight() ;
        }
    }
}

void BStarTree::plot(std::string pictureName, int outLineX,int outLineY) {
    std::fstream f ;
    std::fstream f2 ;
    f.open("gnu.pt",std::ios::out) ;
    f2.open("data.txt",std::ios::out) ;
    f << "reset" << std::endl ;
    f << "set grid" << std::endl ;
    f << "set size square" << std::endl ;

    for ( int i = 0 ; i < blockList.size() ; i++ ) {
        int leftdownX = blockList[i]->getX() ;
        int leftdownY = blockList[i]->getY() ;
        int rightupX = blockList[i]->getX() + blockList[i]->getWidth() ;
        int rightupY = blockList[i]->getY() + blockList[i]->getHeight() ;
        f << "set object " << i+1 << " rectangle from " << leftdownX << "," << leftdownY << " to " 
        << rightupX << "," << rightupY
        <<" lw 1 fc rgb \"black\" fillstyle transparent solid 0.1" << std::endl ;

        f << "set label \"" << blockList[i]->getName() << "\" at " << (leftdownX+rightupX)/2 << "," << (leftdownY+rightupY)/2 
        <<" front center font \",30\"" << std::endl ;
        f2 << leftdownX << ' ' << leftdownY << std::endl;
        f2 << rightupX << ' ' << rightupY << std::endl;
    }
    f << "set arrow from 0," << outLineY << " to " << outLineX << ',' << outLineY << " nohead lc 3 lw 5" << std::endl ;
    f << "set arrow from "<< outLineX <<",0 to " << outLineX << ',' << outLineY << " nohead lc 3 lw 5" << std::endl ;
    f2 << outLineX << ' ' << outLineY << std::endl ;

    f << "set term png size 2000,2000" << std::endl ;
    f << "set output \'"<< pictureName << "\'" << std::endl ;
    f << "plot \'data.txt\' using 1:2 with points pointtype 7 pointsize 3 lc rgb \"green\"" << std::endl ;
    f.close() ;
    f2.close() ;
}



void BStarTree::SA(int widthLimit,int heightLimit,double t,double s) {
    temperature = t ;
    scale = s ;
    setWidthLimit(widthLimit) ;
    setHeightLimit(heightLimit) ;
    int epoch = 0 ;
    while((maxX > widthLimit) || (maxY >heightLimit ) || (maxX == -1) || (maxY == -1)) {
        int choice = rand()%4 ;
        if (choice <= 0) {
            m1Rotate() ;
        } else if (choice <= 1) {
            m5DeleteInsert() ;
        } else if (choice <= 2) {
            m3SwapTwoNodeInf() ;
        } else if (choice <= 3){
            m2SwapChildren() ;
        } else {
            //m4RotatingAll() ;
        }

        epoch++ ;
        if ((epoch % 9) == 0) {
            // std::cout << epoch << std::endl ;
            // std::cout << maxX << ',' << maxY <<':' << widthLimit << ',' << heightLimit <<   std::endl ;
            temperature *= scale ;
            //epoch = 0 ;
        } 
    }
    std::cout <<"SA run "<< epoch <<" times"<< std::endl ;
    std::cout <<"(MaxX,MaxY)     is :"<< maxX << ',' << maxY << std::endl ;
    std::cout <<"(limitX,limitY) is :" << widthLimit << ',' << heightLimit <<   std::endl ;

    if (isInitBest == 1) {
        backtrackToBest() ;
    }
    updatePosition() ;
    // plot(widthLimit,heightLimit);
}


void BStarTree::m1Rotate() {
    int index = rand() % (blockList.size()) ;
    bptr p = blockList[index] ;
    p->rotateBlock();
    updatePosition();

    if (isMove() == 0) {
        p->rotateBlock();
        updatePosition();
    }
}

void BStarTree::m2SwapChildren() {
    int index = rand() % (blockList.size()) ;
    bptr p = blockList[index] ;
    p->changeChildren() ;
    updatePosition();

    if (isMove() == 0) {
        p->changeChildren();
        updatePosition();
    }
}

void BStarTree::m3SwapTwoNodeInf() {
    int index1 = 0 ;
    int index2 = 0 ;
    while (index1 == index2) {
        index1 = rand() % (blockList.size()) ;
        index2 = rand() % (blockList.size()) ;
    }
    bptr p1 = blockList[index1] ;
    bptr p2 = blockList[index2] ;

    std::string n = p1->getName() ;
    p1->setName(p2->getName()) ;
    p2->setName(n) ;

    blockMap[p1->getName()] = p1 ;
    blockMap[p2->getName()] = p2 ;

    int w = p1->getWidth() ;
    p1->setWidth(p2->getWidth()) ;
    p2->setWidth(w) ;

    int h = p1->getHeight() ;
    p1->setHeight(p2->getHeight()) ;
    p2->setHeight(h) ;

    updatePosition();
    if(isMove() == 0){
        n = p1->getName() ;
        p1->setName(p2->getName()) ;
        p2->setName(n) ;

        w = p1->getWidth() ;
        p1->setWidth(p2->getWidth()) ;
        p2->setWidth(w) ;

        h = p1->getHeight() ;
        p1->setHeight(p2->getHeight()) ;
        p2->setHeight(h) ;

        blockMap[p1->getName()] = p1 ;
        blockMap[p2->getName()] = p2 ;

        updatePosition();
    }


}

void BStarTree::m4RotatingAll() {
    for ( int i = 0 ; i < blockList.size() ; i++ ) {
        bptr tmp = blockList[i] ;
        tmp->rotateBlock() ;
        tmp->changeChildren() ;
    }

    updatePosition();
    if (isMove() == 0) {
        for ( int i = 0 ; i < blockList.size() ; i++ ) {
            bptr tmp = blockList[i] ;
            tmp->rotateBlock() ;
            tmp->changeChildren() ;
        }
        updatePosition();
    }
}


void BStarTree::m5DeleteInsert() {
    int operations = 0 ;
    bptr cur = root ;
    bptr p1 = nullptr;
    bptr p1u = nullptr ;
    bptr p1r = nullptr ;

    bptr p2 = nullptr ;
    bptr p2u = nullptr ;
    bptr p2r = nullptr ;

    int index1 = rand() % (blockList.size()) ;

    // select child which is leaf
    if (rand()%2) {
        operations = 1;
    } else {
        operations = 0;
    }

    // randome select start point
    cur = blockList[index1] ;
    bptr tmp = nullptr ;
    while (1) {
        if (operations == 1) {
            tmp = cur->getUp() ;
        } else {
            tmp = cur->getRight() ;
        }

        if ( tmp != nullptr) {
            if (( (tmp->getUp()) == nullptr) && ( (tmp->getRight()) == nullptr)) {
                p1 = cur ;
                break ;
            }
        }

        // Try next
        index1++ ;
        if (index1 >= (blockList.size()) ) {
            index1 = 0 ;
            if (operations == 1) {
                operations = 0 ;
            } else {
                operations = 1 ;
            }
        }
        cur = blockList[index1] ;
    }


    p1 = cur;

    p1u = p1->getUp() ;
    p1r = p1->getRight() ;
    // delete
    if(operations == 1) {
        p1->setUp(nullptr) ;
    } else {
        p1->setRight(nullptr) ;
    }


    int index2 = rand() % (blockList.size()) ;

    cur = blockList[index2] ;
    while (cur == tmp) {
        index2 = rand() % (blockList.size()) ;
        cur = blockList[index2] ;
    }
    
    while((cur->getUp() != nullptr) && (cur->getRight() != nullptr)) {
        if(rand()%2){
            cur = cur->getUp() ;
        } else {
            cur = cur->getRight() ;
        }
    }

    p2 = cur ;
    p2u = p2->getUp() ;
    p2r = p2->getRight() ;

    // insertion
    if((p2->getUp()) == nullptr) {
        if (operations == 1 ) {
            p2->setUp(p1u) ;
        } else {
            p2->setUp(p1r) ;
        }
    } else {
        if (operations == 1 ) {
            p2->setRight(p1u) ;
        } else {
            p2->setRight(p1r) ;
        }
    }

    updatePosition();
    if (isMove() == 0) {
        p1->setUp(p1u) ;
        p1->setRight(p1r) ;
        if (p1 != p2){
            p2->setUp(p2u) ;
            p2->setRight(p2r) ;
        }
        updatePosition();
    }
    




    
}


int BStarTree::isMove() {
    double cost = 0 ;
    int haveMove = 0 ;
    if ((maxX - outLineWidth) > 0 ) {
        cost += static_cast<double>(maxX - outLineWidth) / static_cast<double>(outLineWidth) ;
    }

    if ((maxY - outLineHeight) > 0 ) {
        cost += static_cast<double>(maxY - outLineHeight)/ static_cast<double>(outLineHeight) ;
    }

    if (isInitBest == 0 ) {
        setBSTreeBest() ;
        bestCost = cost ;
        preCost = cost ;
        haveMove = 1 ;
    } else {
        if ( cost < preCost ) {
            if ( cost < bestCost) {
                setBSTreeBest() ;
                bestCost = cost ;
            }
            preCost = cost ;
            haveMove = 1 ;
        } else {
            double deltaCost = cost - preCost ;
            double prob = exp(-deltaCost/temperature) ;
            int x = rand() % 500 ;
            double x_d = static_cast<double>(x) ;
            x_d /= 500 ;
            if ( prob >= x_d ) {
                haveMove = 1 ;
                preCost = cost ;
            } 
        }
    }

    return haveMove ;
}


void BStarTree::writeFile(std::fstream & f) {
    for ( int i = 0 ; i < blockList.size() ; i++ ) {
        bptr p = blockList[i] ;
        f   << p->getName() << " " << p->getX() << " " << p->getY() << " "
            << (p->getX() + p->getWidth()) << " " << (p->getY() + p->getHeight()) << std::endl ;
    }
}

