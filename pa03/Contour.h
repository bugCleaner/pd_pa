#ifndef __CONTOUR_H__
#define __CONTOUR_H__

#include <iostream>


class Contour {
    public :
        Contour() {
            root = new Point() ;
            root->x = 0 ;
            root->y = 0 ;
            root->next = nullptr ;
            root->pre = nullptr ;
        }
        ~Contour() { 
            pptr cur = root ;
            pptr tmp ;
            while(cur != nullptr) {
                tmp = cur ;
                cur = cur->next ;
                delete tmp ;
            }
        }
        /*
        *   reutnr y level
        *   add horizontal contour
        */
        int addHC(int start , int end , int height) {
            pptr cur = root ;
            pptr frontSide = nullptr ;
            pptr lastBeforeEnd = nullptr ;
            pptr deleteStart = nullptr ;
            pptr tmp = nullptr ;

            int maxY = -1 ;
            int rightPointY = -1 ;

            pptr addedPoint = new Point() ;
            addedPoint->x = start ;
            addedPoint->y = -1 ;
            addedPoint->pre = nullptr ;
            addedPoint->next = nullptr ;

            pptr addedPointRight = new Point() ;
            addedPointRight->x = end ;
            addedPointRight->y = -1 ;
            addedPointRight->pre = nullptr ;
            addedPointRight->next = nullptr ;


            // assertion 
            if (cur == nullptr) {
                std::cout << "assertion: addHC meet \" root == nullptr \" " << std::endl ;
                std::cin.get() ;
                return -1 ;
            }

            // Traverse from the leftest point(x==0) to the rightest point
            while(cur != nullptr) {

                // For finding point before start (frontSide->next = addedPoint)
                if (cur->x < start) {
                    frontSide = cur ;
                }

                // Find last point in [0,end] for new a point in right side
                if (cur->x <= end) {
                    lastBeforeEnd = cur ;
                    rightPointY = cur->y ;
                }

                // Find maxY of points in [start,end) 
                if ((cur->x >= start ) && (cur->x < end)) {
                    if (cur->y > maxY){
                        maxY = cur->y ;
                    }
                }
                cur = cur->next ;
            }

            // setting Y of point
            if (maxY == -1) {
                // assertion
                if (frontSide == nullptr) {
                    std::cout << "assertion: addHC meet \" frontSide == nullptr and maxY == -1 \" " << std::endl ;
                    std::cin.get() ;
                }
                maxY = frontSide->y ;
            } else {
                if (frontSide != nullptr) {
                    if (frontSide->next->x > start) {
                        if (frontSide->y > maxY) {
                            maxY = frontSide->y ;
                        }
                    }  
                }
            }

            addedPoint->y = (maxY + height) ;

            addedPoint->pre = frontSide ;
            addedPoint->next = addedPointRight ;

            addedPointRight->pre = addedPoint ;
            addedPointRight->next = lastBeforeEnd->next ;
            addedPointRight->y = lastBeforeEnd->y ;

            cur = root ;
            while(cur != nullptr) {
                if ((cur->x >= start) && (cur->x <= end)) {
                    deleteStart = cur ;
                    break ;
                }
                cur = cur->next ;
            }

            // delete point in [start,end]
            while(deleteStart != nullptr) {
                tmp = deleteStart ;
                deleteStart = deleteStart->next ;
                if ((tmp->x >= start) && (tmp->x <= end)){
                    delete tmp ;
                }
            }

            if (addedPoint->pre != nullptr) {
                addedPoint->pre->next = addedPoint ;
            }
            
            if (addedPointRight->next != nullptr){
                addedPointRight->next->pre = addedPointRight ;
            }


            if (addedPoint->pre == nullptr) {
                root = addedPoint ;
            }

            return maxY ;
        }

        void printList() {
            pptr cur = root ;
            while(cur != nullptr){
                std::cout <<"("<< cur->x << ',' << cur->y << ')' ;
                cur = cur->next; 
            }
            std::cout << std::endl;

        }

    private :
        class Point ;
        typedef Point* pptr ;
        pptr root ;

        class Point {
            public :
                Point() { x = 0; y = 0; pre = nullptr; next = nullptr; }
                int x ;
                int y ;
                pptr pre ;
                pptr next ;
        } ;

} ;



#endif
