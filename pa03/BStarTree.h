#ifndef __BSTARTREE_H__
#define __BSTARTREE_H__

#include <string>
#include <vector>
#include <map>
#include "Contour.h"
#include <limits>
#include <fstream>

class BStarTree {
    public :
        BStarTree() ;
        ~BStarTree() ;
        // Add terminals
        void addTerminal(std::string ,int ,int );
        double getTerminalX(std::string ) ;
        double getTerminalY(std::string ) ;
        int isTerminal(std::string ) ;
        double getBlockX(std::string ) ;
        double getBlockY(std::string ) ;
        int isBlock(std::string ) ;
        // Add net
        void addNet(std::vector<std::string> &) ;
        double getTotalWireLength() ;
        // Add block to B*-tree
        void addBlock(std::string,int,int) ;
        // Update (x,y) of each block
        void updatePosition() ;
        // Backtrack to best record result
        void backtrackToBest() ;
        // Save the current B*-tree as best record
        void setBSTreeBest() ;

        // Three operations
        void m1Rotate() ;
        void m2SwapChildren() ;
        // This function will change the <name,bptr> pair in blockMap
        void m3SwapTwoNodeInf() ;

        void m4RotatingAll() ;

        void m5DeleteInsert() ;

        // Set/Get the limit of range of X
        void setWidthLimit(int limit) { this->outLineWidth = limit ;}
        int getWidthLimit() { return this->outLineWidth ;}
        void setHeightLimit(int limit) { this->outLineHeight = limit ;}
        int getHeightLimit() { return this->outLineHeight ;}

        // produce plot file for gnuplot
        void plot(std::string,int,int);

        // Simulated annealing
        void SA(int,int,double,double);

        int getMaxX() {return this->maxX ;}
        int getMaxY() {return this->maxY ;}
        void writeFile(std::fstream & ) ;

    private :
        // Predefinition
        class Block ;
        class BSTPoint ;
        class NET ;
        typedef Block* bptr ;
        typedef BSTPoint* bstpptr ;
        typedef NET* nptr ;

        // Used for updatePosition
        void updateBlockRecur(int,bptr,Contour&);
        // To perform update core of SA
        int isMove() ;
        // If no blocks , then it is 1
        char isEmpty ;
        // To check if initialize best record or not
        char isInitBest ;
        // Parameters for simulated annealing
        double temperature ;
        double scale ;
        // Number of blocks
        int blockCount ;
        // The outLine limit for horizontal contour
        int outLineWidth ;
        int outLineHeight ;
        // To record the max X and Y in each position updating 
        int maxX ;
        int maxY ;
        // The best cost of simulated annealing
        double bestCost ;
        double preCost ;
        // Root of B*-Tree
        bptr root ;
        // The root of best solution (for recover)
        bptr bestRoot ;
        // To mapping the name to address of block
        std::map<std::string, bptr> blockMap;
        // The same objects in blockMap were recorded in vector for updating blockMap (Mainly used in function:backtrackToBest) 
        std::vector<bptr> blockList;

        // terminals
        std::map<std::string, bstpptr> TerminalMap;

        // NETs
        std::vector<nptr> netVector;

        // Data structure for B*-Tree
        class Block {
            public :
                Block(std::string n,int w,int h) {
                    this->name = n ;
                    this->width = w ;
                    this->height = h ;
                    this->x = 0 ;
                    this->y = 0 ;
                    this->right = nullptr ;
                    this->up = nullptr ;
                    setBest() ;
                } 
                ~Block() {/*Nothing to do*/ ;} 
                void rotateBlock() {
                    int tmp = this->width;
                    this->width = this->height ;
                    this->height = tmp ;
                }
                void changeChildren() {
                    bptr p = this->up ;
                    this->up = this->right ;
                    this->right = p ;
                }
                void setName(std::string n) {this->name = n ;}
                std::string getName() { return this->name ;}
                void setWidth(int w) { this->width = w ;}
                void setHeight(int h) { this->height = h ;}
                int getWidth() { return this->width ;}
                int getHeight() { return this->height ;}
                void setX(int x_in) { this->x = x_in ;}
                void setY(int y_in) { this->y = y_in ;}
                int getX() { return this->x ;}
                int getY() { return this->y ;}
                void setRight(bptr p) { this->right = p ;}
                void setUp(bptr p) { this->up = p ;}
                bptr getRight() { return this->right ;}
                bptr getUp() { return this->up ;}
                void setBest() ;
                void backToBest() ;
            private :
                // block name
                std::string name ;
                // variables
                int width ;
                int height ;
                // left-down cornor
                int x ;
                int y ;
                // tree ptr
                bptr right ;
                bptr up ;

                //best
                std::string best_name ;
                int best_width ;
                int best_height ;
                int best_x ;
                int best_y ;
                bptr best_right ;
                bptr best_up ;
        } ;

        class BSTPoint {
            public :
                BSTPoint(std::string s , double x_in , double y_in) {
                    this->name = s ;
                    this->x = x_in ;
                    this->y = y_in ;
                } 
                void setX(double x_in) {this->x = x_in ;}
                void setY(double y_in) {this->y = y_in ;}
                void setName(std::string s) {this->name = s ;}
                std::string getName() {return this->name ;}
                double getX() {return this->x ;}
                double getY() {return this->y ;} 
            private :
                std::string name ;
                double x,y ;


        } ;

        class NET {
            public :
                NET() { reset() ;}
                void reset() {
                    maxX = std::numeric_limits<double>::min() ; 
                    maxY = std::numeric_limits<double>::min() ; 
                    minX = std::numeric_limits<double>::max() ; 
                    minY = std::numeric_limits<double>::max() ;
                }
                double maxX , minX;
                double maxY , minY;
                std::vector<std::string> blocks;
                std::vector<std::string> terminals;

        } ;


} ;
#endif
