#include "FloorPlanner.h"
#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>
#include <iomanip>

class parsingBlock {
    public :
        parsingBlock(std::string n , int w , int h ){
            name = n ;
            width = w ;
            height = h ;
            area = w*h ;
        }

        std::string name ;
        int width ;
        int height ;
        int area ; 
} ;

bool myCmp (const parsingBlock& b1 , const parsingBlock& b2 ) {
    return b1.area > b2.area ;
}

void FloorPlanner::parsingBlockFile(std::string inputFileName) {
    std::fstream f ;
    f.open(inputFileName,std::ios::in) ;
    if (!f) {
        std::cout << "Assertion: FloorPlanner::parsingBlockFile can not open " << inputFileName << std::endl ;
        std::cin.get() ;
        return ;
    }

    std::string token ;

    enum Reading {prestage,blocks,terminals,end};

    Reading stage = prestage ;

    std::vector<parsingBlock> blockVector;

    while(f>>token) {
        if (stage == prestage) {
            if (token == "Outline:") {
                f >> outlineWidth ;
                f >> outlineHeight ;
                bt.setHeightLimit(outlineHeight) ;
                bt.setWidthLimit(outlineWidth) ;
            } else if (token == "NumBlocks:") {
                f >> blockNum ;
            } else if (token == "NumTerminals:") {
                f >> terminalNum ;
                stage = blocks ;
            } else {
                std::cout << "Assertion: Bugs in prestage" << std::endl ;
            }
        } else if ( stage == blocks ){
            for ( int i = 0 ; i < blockNum ; i++ ) {
                int w,h ;
                f >> w ;
                f >> h ;
                blockVector.push_back(parsingBlock(token,w,h)) ;
                
                if ( i != (blockNum-1)){
                    f >> token ;
                }
            }

            std::sort(blockVector.begin(),blockVector.end(),myCmp) ;

            for ( int i = 0 ; i < blockNum ; i++ ) {
                // std::cout << blockVector[i].name << " : " << blockVector[i].area << std::endl ; 
                bt.addBlock(blockVector[i].name,blockVector[i].width,blockVector[i].height) ;
            }

            blockVector.clear();
            stage = terminals ;

        } else if ( stage == terminals) {
            for ( int i = 0 ; i < terminalNum ; i++ ) {
                double x,y ;
                std::string gs ;
                f >> gs ;
                f >> x ;
                f >> y ;
                // std::cout << "terminals " << i << ":"<< token <<"("<< x <<"," << y << ")"<< std::endl ;
                bt.addTerminal(token,x,y) ;
                if ( i != (terminalNum-1)){
                    f >> token ;
                }
            }
            stage = end ;
        } else {
            std::cout << "END" << std::endl ;
            break ;
        }
    }

    f.close() ;
}


void FloorPlanner::parsingNetFile(std::string inputFileName) {
    std::fstream f ;
    f.open(inputFileName,std::ios::in) ;
    if (!f) {
        std::cout << "Assertion: FloorPlanner::parsingNetFile can not open " << inputFileName << std::endl ;
        std::cin.get() ;
        return ;
    }

    std::string token ;

    f >> token ; //NumNets:
    f >> netNum ;
    for ( int i = 0 ; i < netNum ; i++) {
        f>>token ;
        int tN ;
        f >> tN ;
        std::vector<std::string> v;
        for ( int j = 0 ; j < tN ; j++) {
            f>>token ;
            v.push_back(token) ;
        }
        bt.addNet(v) ;
        v.clear() ;
    }


    f.close() ;
}



void FloorPlanner::writeOutputFile(std::string outputFileName) {
    std::fstream f ;
    f.open(outputFileName,std::ios::out) ;
    if (!f) {
        std::cout << "Assertion: FloorPlanner::writeOutputFile can not open " << outputFileName << std::endl ;
        std::cin.get() ;
        return ;
    }

    double A = static_cast<double>(bt.getMaxX() * bt.getMaxY()) ;
    double W = bt.getTotalWireLength() ;
    f << std::setprecision(10) << (A*alphaValue + (1.0-alphaValue)*W) << std::endl ;
    f << std::setprecision(10) << W << std::endl ;
    f << std::setprecision(10) << A << std::endl ;
    f << std::setprecision(10) << bt.getMaxX() << " " << std::setprecision(10) << bt.getMaxY() << std::endl ;
    t1 = clock() ;
    f << std::setprecision(10) << (t1-t0)/double(CLOCKS_PER_SEC) << std::endl ; 
    bt.writeFile(f) ;


    f.close() ; 
}



